import {Link} from 'react-router-dom';
import {Navbar, Container, Nav} from 'react-bootstrap';
import {useContext, useState} from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){

	const {user} = useContext(UserContext);
	const [expanded, setExpanded] = useState(false);
		

	return(
		<Navbar expanded={expanded} bg="secondary" expand="lg">
		    <Container>
		        <Navbar.Brand as={Link} to="/" style={{color: "white" }}>Shortstop</Navbar.Brand>
		        <Navbar.Toggle onClick={() => setExpanded(!expanded)} aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		        	<Nav className="me-auto">
		            	<Nav.Link onClick={() => setExpanded(false)} as={Link} to="/" style={{color: "white" }}>Home</Nav.Link>
		        {    	
		        	(user.isAdmin === false)?
		            	<Nav.Link onClick={() => setExpanded(false)} as={Link} to="/products" style={{color: "white" }}>Products</Nav.Link>
		            :
		            <>
		            	<Nav.Link onClick={() => setExpanded(false)} as={Link} to="/dashboard" style={{color: "white" }}>Admin Dashboard</Nav.Link>
		            </>
		        }
		        {
		        	(user.id !== null) ?
		        	<>
		            	<Nav.Link onClick={() => setExpanded(false)} as={Link} to="/userprofile" style={{color: "white" }}>Profile</Nav.Link>
		            	<Nav.Link onClick={() => setExpanded(false)} as={Link} to="/logout" style={{color: "white" }}>Logout</Nav.Link>
		            </>
		        	:	
		            <>
		            	<Nav.Link onClick={() => setExpanded(false)} as={Link} to="/login" style={{color: "white" }}>Login</Nav.Link>
		            	<Nav.Link onClick={() => setExpanded(false)} as={Link} to="/register" style={{color: "white" }}>Register</Nav.Link>
		            </>
		        }
		          	</Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
	)
};