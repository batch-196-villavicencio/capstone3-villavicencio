import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}){
	const {_id, name, description, price} = productProp

	return(
			<Card className="cardHighlight p-3 mb-3">
				<Card.Body>
					<Card.Title className="fw-bold">
						{name}
					</Card.Title>
					<Card.Subtitle>Product Description</Card.Subtitle>
					<Card.Text>
						{description}
					</Card.Text>
					<Card.Subtitle>Price</Card.Subtitle>
					<Card.Text>
						{price}
					</Card.Text>
					<Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>
				</Card.Body>
			</Card>
	)
}