import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useParams, Link, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){

	const {user} = useContext(UserContext);
	const history = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const {productId} = useParams();
	const [total, setTotal] = useState(0);
	const [qty, setQty] = useState(1)


	useEffect(() => {
		fetch(`https://intense-garden-72208.herokuapp.com/products/viewOne/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})
	}, [productId]);

	useEffect(() => {
		if (qty === undefined || qty === 0){
			setTotal(0)
		} else {
			setTotal(qty * price)
		}
	},[qty,price])

	function checkOut(e){
		e.preventDefault();

		Swal.fire({
			title: 'Check out?',
			text: "This will check out your product selection",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then((result) => {
			if (result.isConfirmed) {
				fetch('https://intense-garden-72208.herokuapp.com/orders/checkOut',{
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						products:
						[
							{
								productId:productId,
								quantity:qty
							}
						]
					})
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire(
						'Successful!',
						'Purchase is successful',
						'success'
					).then(function(){
						history("/userprofile");
					})
				})
			}
		})
	}

	return(

		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Card.Subtitle>Quantity:</Card.Subtitle>
							<input id='qty' type='number' style={{width:"3rem"}} defaultValue="1" onChange={e=> setQty(e.target.value)}/>
							<br/>
							<Card.Subtitle className="mt-3">Total Amount:</Card.Subtitle>
							<Card.Text>Php {total}</Card.Text>
							{ user.id !== null ?
								<Button variant="primary" className="mt-2" onClick={e => checkOut(e)}>Buy Now</Button>
								:
								<Link className="btn btn-danger mt-2" to="/login">Log In</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>


	)
}