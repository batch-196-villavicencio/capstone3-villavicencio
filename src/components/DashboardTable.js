import {Card, Button, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {useState} from 'react'


export default function DashboardTable({tableProp}){
	const {_id, name, description, price, isActive} = tableProp
	const [show, setShow] = useState(false)
	const handleClose = () => setShow(false)
	const handleShow = () => setShow(true)
	const [newName, setNewName] = useState(name)
	const [newDescription, setNewDescription] = useState(description)
	const [newPrice, setNewPrice] = useState(price)

	function updateProduct(e){
		e.preventDefault();

		fetch(`https://intense-garden-72208.herokuapp.com/products/update/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: newName,
				description: newDescription,
				price: newPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: 'Updated Product Successfully',
					icon: 'success',
					text: 'Thank you for updating product'
				}).then((result) => {
					window.location.reload()
				})
			} else {
				Swal.fire({
					title: 'Update Product Failed!',
					icon: 'error',
					text: 'Something went wrong, try again later!'
				})
			}
		})
	}

	function activateProduct(e){
		e.preventDefault();

		Swal.fire({
			title: 'Are you sure?',
			text: "This will activate the product",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Activate'
		}).then((result) => {
			if (result.isConfirmed) {
				fetch(`https://intense-garden-72208.herokuapp.com/products/activate/${_id}`,{
					method: 'PUT',
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire(
						'Activated!',
						'Product has been activated',
						'success'
					).then(function(){
						window.location.reload()
					})
				})
			}
		})
	}


	function deactivateProduct(e){
		e.preventDefault();

		Swal.fire({
			title: 'Are you sure?',
			text: "This will deactivate the product",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Deactivate'
		}).then((result) => {
			if (result.isConfirmed) {
				fetch(`https://intense-garden-72208.herokuapp.com/products/archive/${_id}`,{
					method: 'DELETE',
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire(
						'Deactivated!',
						'Product has been deactivated',
						'success'
					).then(function(){
						window.location.reload();
					})
				})
			}
		})
	}


	return(
		<>
			<Card style={{ width: '15rem'}}>
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Text>{description}</Card.Text>
					<Card.Text>Price: Php {price}</Card.Text>
				</Card.Body>
				<Card.Footer>	
					<Button variant="primary" onClick={handleShow}>Update</Button>
					{ isActive === true ?							
						<Button variant="danger" onClick={deactivateProduct}>Deactivate</Button>
						:
						<Button variant="success" onClick={activateProduct}>Activate</Button>
					}
				</Card.Footer>
			</Card>

			<Modal
				show={show}
				onHide={handleClose}
				backdrop="static"
				keyboard={false}
			>
				<Modal.Header closeButton>
					<Modal.Title>Update Product</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form onSubmit={e => updateProduct(e)}>
						<Form.Group controlId="newProductName">
							<Form.Label className="mt-3">Product Name</Form.Label>
							<Form.Control
								type="text"
								required
								defaultValue={name}
								onChange={e=> setNewName(e.target.value)}
							/>
						</Form.Group>

						<Form.Group controlId="newProductDescription">
							<Form.Label className="mt-3">Product Description</Form.Label>
							<Form.Control
								type="text"
								required
								defaultValue={description}
								onChange={e=> setNewDescription(e.target.value)}
							/>
						</Form.Group>

						<Form.Group controlId="newProductPrice">
							<Form.Label className="mt-3">Price</Form.Label>
							<Form.Control
								type="number"
								required
								defaultValue={price}
								onChange={e=> setNewPrice(e.target.value)}
							/>
						</Form.Group>

						<Button className="mt-3 mb-5" variant="success" type="submit" id="updateSubmitBtn">
							Update
						</Button>
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={handleClose}>
						Close
					</Button>
				</Modal.Footer>
			</Modal>
		</>
	)
}