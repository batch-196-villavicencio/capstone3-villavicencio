import {Link} from 'react-router-dom';
import {Row, Col, Container} from 'react-bootstrap';

export default function Banner(){


	return (
		<Container>
			<Row className="justify-content-md-center">
				<Col className ="p-5" md="auto">
					<h1 style={{textAlign: "center", color: "white", backgroundColor: "black"}}>Shortstop</h1>
					<p style={{textAlign: "center", color: "white", backgroundColor: "black"}}>I like shorts! They're comfy and easy to wear!</p>
					<Link style={{width: "100%"}} to="/products" className="btn btn-primary">Go to Shop!</Link>
				</Col>
			</Row>
		</Container>
	)
};