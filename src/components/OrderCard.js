import {Card} from 'react-bootstrap';
import {useState, useEffect} from 'react';


export default function OrderCard({orderProp}){
	const {totalAmount, purchasedOn, products} = orderProp
	const [name, setName] = useState("");
	const [quantity, setQuantity] = useState("");
	
	useEffect(() => {
		for(let x=0;x < products.length;x++){
			fetch(`https://intense-garden-72208.herokuapp.com/products/viewOne/${products[x].productId}`)
			.then(res => res.json())
			.then(data => {
				setName(data.name);
				setQuantity(products[x].quantity);

			})
		}
	},)


	return(

		<Card className="mt-3">
			<Card.Title>Order</Card.Title>
			<Card.Subtitle>Product</Card.Subtitle>
			<Card.Text>
				{name}
			</Card.Text>
			<Card.Subtitle>Quantity</Card.Subtitle>
			<Card.Text>
				{quantity}
			</Card.Text>
			<Card.Subtitle>Total Amount</Card.Subtitle>
			<Card.Text>
				{totalAmount}
			</Card.Text>
			<Card.Subtitle>Date Purchased</Card.Subtitle>
			<Card.Text>
				{purchasedOn}
			</Card.Text>
		</Card>

	)
}