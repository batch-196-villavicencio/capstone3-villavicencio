import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

	const {user} = useContext(UserContext);
	const history = useNavigate();
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobile, setMobileNo] = useState('')
	const [email, setEmail] = useState('')
	const [pass, setPass] = useState('')
	const [isActive, setIsActive] = useState(false)

	function registerUser(e){
		e.preventDefault();

		fetch('https://intense-garden-72208.herokuapp.com/users/checkEmail',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: "Email is already in use",
					icon: "info",
					text: "The email you are trying to register already exists in our database"
				});
			} else {
				fetch('https://intense-garden-72208.herokuapp.com/users',{
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobile,
						email: email,
						password: pass
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data){
						Swal.fire({
							title: 'Registration Successful!',
							icon: 'success',
							text: 'Thank you for registering'
						});

						history("/login");
					} else {
						Swal.fire({
							title: 'Registration Failed',
							icon: 'error',
							text: 'Something went wrong. Please try again later'
						});
					};
				})
			}
		})

		setEmail('');
		setPass('');
		setFirstName('');
		setLastName('');
		setMobileNo('');

	}

	useEffect(() => {
		
		if (email !== '' && pass !== '' && firstName !== '' && lastName !== '' && mobile !== '' && mobile.length === 11) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, pass, firstName, lastName, mobile]);

	return(

		(user.id !== null)?
			<Navigate to="/"/>
		:
		<>
		<h1 className="mt-5">Register Here:</h1>
		<Form onSubmit={e => registerUser(e)}>
			<Form.Group controlId="userFirstName">
				<Form.Label className="mt-3">First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your first name"
					required
					value= {firstName}
					onChange= {e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userLastName">
				<Form.Label className="mt-3">Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your last name"
					required
					value= {lastName}
					onChange= {e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userMobile">
				<Form.Label className="mt-3">Mobile No.</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your mobile number"
					required
					value= {mobile}
					onChange= {e => setMobileNo(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label className="mt-3">Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value= {email}
					onChange= {e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label className="mt-2">Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value= {pass}
					onChange= {e => setPass(e.target.value)}
				/>
			</Form.Group>


			{ isActive ?
				<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
							Register
				</Button>
				:
				<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>
							Register
				</Button>
			}
		</Form>
		</>


	)
}