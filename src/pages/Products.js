import ProductCard from '../components/ProductCard';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Products(){
	
	const {user} = useContext(UserContext);
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch("https://intense-garden-72208.herokuapp.com/products/viewAllActive")
		.then(res => res.json())
		.then(data => {
			
			setProducts(data.map(product =>{
				return(
					
					<ProductCard key={product._id} productProp = {product}/>
				)
			}));

		})

	}, [])

	return(
		(
			user.isAdmin === true ?
				<Navigate to="/dashboard"/>
			:
			<>	
			<h1 className="mt-5">Available Products:</h1>
			{products}
			</>	
		)	
		


	)
}