import {useState, useEffect, useContext} from 'react';
import {Navigate, Link} from 'react-router-dom'
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Login(){

	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState('')
	const [pass, setPass] = useState('')
	const [isActive, setIsActive] = useState(false)

	function loginUser(e){
		e.preventDefault();

		fetch('https://intense-garden-72208.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: pass
			})
		})
		.then(res => res.json())
		.then(data => {

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Shortstop!"
				})
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your credentials"
				})

			}
		})

		setEmail('');
		setPass('');
	}

	const retrieveUserDetails = (token) => {

		fetch('https://intense-garden-72208.herokuapp.com/users/userDetails', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if(email !== '' && pass !== '') {
			setIsActive(true);
		} else {
			setIsActive(false)
		}
	}, [email, pass]);

	return(

		(user.id !== null)?
			<Navigate to="/"/>
		:
		<>
		<h1 className="mt-5">Login</h1>
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group controlId="userLoginEmail">
				<Form.Label className="mt-3">
					Email Address
				</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value={email}
					onChange= {e => setEmail(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="pass">
				<Form.Label className="mt-2">
					Password
				</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					required
					value={pass}
					onChange= {e => setPass(e.target.value)}
				/>

			</Form.Group>
				<p>Not yet registered? <Link to="/register">Register Here</Link></p>

				{ isActive ?
					<Button className="mt-2 mb-5" variant="success" type="submit" id="submitLoginBtn">
						Login
					</Button>
					:
					<Button className="mt-2 mb-5" variant="danger" type="submit" id="submitLoginBtn">
						Login
					</Button>
				}

		</Form>
		</>

	)
}