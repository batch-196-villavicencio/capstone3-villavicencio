import {Button, Modal, Form, Row} from 'react-bootstrap';
import DashboardTable from '../components/DashboardTable'
import {useState, useContext, useEffect} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Dashboard(){

	const [products, setProducts] = useState([]);
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	const {user} = useContext(UserContext);
	const [name, setName] = useState('');
	const [desc, setDesc] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(false);


	useEffect(() => {
		fetch("https://intense-garden-72208.herokuapp.com/products/viewAll",{
			method: 'GET',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setProducts(data.map(product => {
				return(
					<DashboardTable key={product._id} tableProp = {product}/>
				)
			}));
		})
	}, [])

	function addProduct(e){
		e.preventDefault();

		fetch('https://intense-garden-72208.herokuapp.com/products',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: desc,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: 'Product Creation Success!',
					icon: 'success',
					text: 'Thank you for creating new product'
				}).then((result) => {
					window.location.reload()
				})
			} else {
				Swal.fire({
					title: 'Product Creation Failed!',
					icon: 'error',
					text: 'Something went wrong, try again later'
				})
			}
		})
	}



	useEffect(() => {

		if(name !== '' && desc !== '' && price > 0) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, desc, price])

	return(
		
		(user.isAdmin === false)?
			<Navigate to="/"/>
		:
		<>
			<h1 style={{textAlign: "center"}} className="mt-5">ADMIN DASHBOARD</h1>

			<Button style={{width: "100%"}} variant="primary" onClick={handleShow}>
				Add New Product
			</Button>

			<Modal
				show={show}
				onHide={handleClose}
				backdrop="static"
				keyboard={false}
			>
				<Modal.Header closeButton>
					<Modal.Title>Add Product</Modal.Title>
				</Modal.Header>
				<Modal.Body>
				
					<Form onSubmit={e => addProduct(e)}>
						<Form.Group controlId="productName">
							<Form.Label className="mt-3">Product Name</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter name of product"
								required
								value= {name}
								onChange= {e => setName(e.target.value)}
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label className="mt-3">Product Description</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter product description"
								required
								value= {desc}
								onChange= {e => setDesc(e.target.value)}
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label className="mt-3">Price</Form.Label>
							<Form.Control
								type="number"
								placeholder="Enter price of product"
								required
								value= {price}
								onChange= {e => setPrice(e.target.value)}
							/>
						</Form.Group>

						{ isActive ?
							<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
										Create
							</Button>
							:
							<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>
										Create
							</Button>
						}
					</Form>

				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={handleClose}>
						Close
					</Button>
				</Modal.Footer>
			</Modal>
			<h1 style={{textAlign: "center"}} className="mt-5">Product List</h1>
			<Row>
				{products}
			</Row>
		</>
	)
}