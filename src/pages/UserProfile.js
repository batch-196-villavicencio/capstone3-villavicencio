import OrderCard from '../components/OrderCard';

import {useState, useEffect} from 'react';
import {ListGroup} from 'react-bootstrap';

export default function UserProfile(){

	
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobile, setMobileNo] = useState("");
	const [isAdmin, setIsAdmin] = useState(false);
	const [orders, setOrders] = useState([]);


	
	useEffect(() => {
		fetch('https://intense-garden-72208.herokuapp.com/users/userDetails', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setFirstName(data.firstName);
			setLastName(data.lastName);
			setEmail(data.email);
			setMobileNo(data.mobileNo);
			setIsAdmin(data.isAdmin);

		})
	}, []);

	useEffect(() => {
		fetch('https://intense-garden-72208.herokuapp.com/orders/viewLoginUserOrder', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setOrders(data.map(order =>{
				return(
					<OrderCard key={order._id} orderProp = {order}/>
				)
			}))
		})
	}, [])


	return(

		<>
			<h1 className="mt-5">User Profile</h1>
			<ListGroup className="mt-3">
				<ListGroup.Item>Name: {firstName} {lastName}</ListGroup.Item>
				<ListGroup.Item>Email: {email}</ListGroup.Item>
				<ListGroup.Item>Mobile No.: {mobile}</ListGroup.Item>
				{ isAdmin === true ?
					<ListGroup.Item>Role: Admin</ListGroup.Item>
				:
					<ListGroup.Item>Role: User</ListGroup.Item>	
				}
			</ListGroup>
		

			<h3 className="mt-5">Last Order/s</h3>
			{orders}
			


		</>
	)
}