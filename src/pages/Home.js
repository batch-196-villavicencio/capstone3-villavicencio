import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import background from '../images/comfy-shorts.png';

export default function Home(){

	const getBackground={
		backgroundImage: `url(${background})`,
		height: '100vh',
		backgroundSize: 'contain',
		backgroundRepeat: 'no-repeat'
	}

	return(
		<div style={getBackground}>
			<Banner/>
			<Highlights/>
		</div>

	)
}